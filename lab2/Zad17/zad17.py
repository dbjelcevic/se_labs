from bs4 import BeautifulSoup
import requests
import lxml

soup = BeautifulSoup(requests.get("https://www.nytimes.com/").text, "lxml")

for title in soup.find_all(class_="story-heading"):
    print(title.get_text(strip=True))