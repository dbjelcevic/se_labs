import json
from collections import Counter
from bokeh.plotting import figure,show,output_file

month = []
num_to_string = {
	1: "January",
	2: "February",
	3: "March", 
	4: "April",
	5: "May",
	6: "June",
	7: "July",
	8: "August",
	9: "September",
	10: "October",
	11: "November",
	12: "December"
}
with open("birthdays.json", "r") as f:
    birthdays = json.load(f)

for x in birthdays.values():
    month.append(num_to_string[int(x.split('/')[0])])
countermonths=Counter(month)
x_categories=[]
x=[]
y=[]
print("_________________________")
print('month'.ljust(10),'|','count')
print("_________________________")
for key,value in countermonths.items():
    x.append(key)
    y.append(value)
    print(key.ljust(10),'|',value)
print("_________________________")
output_file("plot.html")
for key,value in num_to_string.items():
    x_categories.append(value)
p=figure(x_range=x_categories)
p.vbar(x=x,top=y,width=0.5)
show(p)