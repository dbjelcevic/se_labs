u1 = input("Player 1, please enter your answer: ")
u2 = input("Player 2, please enter your answer: ")

if (u1 == "rock") or (u1 == "paper") or (u1 == "scissors"):
    if (u2 == "rock") or (u2 == "paper") or (u2 == "scissors"):
        if u1 == u2:
            print("It's a tie.")
        elif u1 == "rock":
            if u2 == "paper":
                print("paper wins.")
            elif u2 == "scissors":
                print("rock wins.")
        elif u1 == "paper":
            if u2 == "scissors":
                print("scissors wins.")
            elif u2 == "rock":
                print("paper wins.")
        elif u1 == "scissors":
            if u2 == "rock":
                print("rock wins.")
            elif u2 == "paper":
                print("scissors wins.")
    else:
        print("Invalid input! You did not enter rock, paper or scissors.")
else:
    print("Invalid input! You did not enter rock, paper or scissors.")