def search(arr,x):
    l = 0
    r = len(arr) - 1
    while l<= r:
        mid = (l+r)//2
        if arr[mid]==x:
            return mid
        elif arr[mid] < x:
            l = mid + 1
        else:
            r = mid-1
    return -1

lst = [1, 3, 5, 30, 42, 43, 500]
x = 45
result = search(lst, x)
if result >= 0:
    print("{} is at index {}".format(lst[result], result))
else:
    print("Search term not found")

