grades = { "Pero": [2,3,3,4,3,5,3],
           "Djuro" : [4,4,4],
           "Marko" : [3,3,2,3,5,1]
           }
           
# Ispisi ime studenta koji ima najvisi prosjek. Zadatak treba proci kroz 
# sve studente, izracunati prosjek i ispisati ime studenta s navisim prosjekom.

# -- vas kod ide ispod -- #

avgmax = 0
maximum = ""
for student in grades:
    average = (sum(grades[student])/len(grades[student]))
    if average > avgmax:
        avgmax = average
        maximum = student
print(maximum)

